// npm init
/*
-triggering this command will prompt the user for different settings that will define the application
-using this command will make a "package.json" in our repo
-package.json tracks the version f our applic depending on the settings we habe set. also, we can see the dependecies that we have installed inside of this file (ano na ang na-install mong technology sa application) 

*/

// npm install express
/*
- after triggering this command, express will now be listed as a "dependency". this can be seen in the "package.json" file under the "dependencies" property
- so wag na wag ieedit itong dependencies
-installing any dependency using npm will result in creating"node_modules" folder and package-lock.json
	"node_modules" directory should be left on the local repository and should not be pushed to gitlab
		- some of the hosting sites will fail to load once it found the "node_modules" inside the repo.
		Also, it takes too much time to commit
	"node_modules" is also where the dependencies needed files are stored
*/

// automatic nag-uupdate si package.json depende sa iniinstall mo

// we need now the express module since in this dependency, it has already nuilt in codes that will allow the dev to creaet a server faster
const express = require("express");
// express() - allows us to create our server using express
// simply put, the app variable is our server (ito yung counterpart ni const server = createServer)
const app = express();
// set up port variable, wedeng ibang port
const port = 3000;

// app.use lets the server to handle data from requests
// app.use(express.json()) - allows the app to read and handle json data types
// methods used from express middlewares 
	 //middleware - software that provides common services for the application 
app.use(express.json());

// app.use(express.urlencoded({extended:true})) - allows the server to read data from forms
// by default, info received from the url can only be received as a string or an array 
// but using extended:true for the argument allows us to receive info from other data types such as objects which we will use throughout our application
app.use(express.urlencoded({extended: true}));

// Section - Routes
// GET Method
/*
Express has methods corresponding to each HTTP methods. The route below expects to receive a GET request at the base URI "/"
*/
app.get("/",(req, res)=>{
	res.send("Hello World");
})

app.get("/hello",(req, res)=>{
	res.send("Hello from /hello endpoint");
});
// pwede bang mutilpe uri? pwede, ipapasok mo sila sa array, as long as same content/response/message but using different uris lang:
app.get(["/hello","test"],(req, res)=>{
	res.send("Hello from /hello endpoint");
});

// POST
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// create if-else statement that if the firstName and last Name are not null, the object will be pushed into the users array. Then send a response that says sign up was successful; Else, the server will send a message that lastName and firstName should have inputs
// let users = [];
// app.post("/signup", (req,res)=>{
// 	console.log(req.body);
// 	if(req.body.firstName !==""&&req.body.lastName!==""){
// 		users.push(req.body);
// 		res.send(`User ${req.body.firstName} ${req.body.lastName} has signed up successfully.`);
// 	}
// 	else {
// 		res.send("Please input BOTH firstName and lastName.");
// 	};
// });

// app.put("/signup", (req,res)=>{
// 	console.log(req.body);
// 	if(req.body.firstName !==""&&req.body.lastName!==""){
// 		users.push(req.body);
// 		res.send(`User ${req.body.firstName} ${req.body.lastName} has signed up successfully.`);
// 	}
// 	else {
// 		res.send("Please input BOTH firstName and lastName.");
// 	};
// });
// // Kung nag-update ng name kasi nagpakasal:
// app.put("/change-lastName", (req,res)=>{
// 	let message;

// 	for (let i=0; i<users.length; i++){
// 		// checks if the user is existing in the array
// 		if(req.body.firstName==users[i].firstName)
// 			// will change the value
// 			{users[i].lastName=req.body.lastName;
// 			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
// 				console.log(users);
// 			// may break para iterminate yung loop kahit yung next iteration niya ay true pa rin. terminate na kasi may match na
// 			break;
// 		}else{
// 			message="User does not exist"
// 		}
// 	}
// 	res.send(message);
// });
// tells our server to listen to the port
// if the port is accessed, we can run the server,
// returns a message to confirm that the server is running
// app.listen(port, ()=>console.log(`Server is running at ${port}`));


// SECTION- ACTIVITY


// 1. Create a GET route that will access the "/home" route that will print out a simple message.

app.get("/home",(req, res)=>{
	res.send("Welcome to the home page!");
});

// 2. Process a GET request at the "/home" route using postman. - OK


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

// Mock Database
let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "janesmith",
		"password": "janesmith123"
	}

];

app.get("/users",(req, res)=>{
	res.send(users);
});

// 4. Process a GET request at the "/users" route using postman.

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.

app.delete("/delete-user", (req, res) => {
   res.send(`User has been deleted.`);
});

// 6. Process a DELETE request at the "/delete-user" route using postman.

// 7. Export the Postman collection and save it inside the root folder of our application.

app.listen(port, ()=>console.log(`Server is running at ${port}`));